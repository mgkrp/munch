const passport = require("passport");
const GoogleStrategy = require("passport-google-oauth20").Strategy;
const keys = require("../config/keys");
const knex = require("../db/knex");

passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser((id, done) => {
  knex("users")
    .where({ id })
    .first()
    .then(user => {
      done(null, user);
    })
    .catch(err => {
      done(err, null);
    });
});

async function findGoogleUser(googleProfile) {
  return await knex("users")
    .where("g_id", googleProfile.id)
    .first();
}

async function saveGoogleUser(profile) {
  return await knex("users")
    .insert({
      g_id: profile.id,
      first_name: profile.name.givenName,
      last_name: profile.name.familyName,
      email: profile.emails[0].value
    })
    .returning("*")
    .get(0);
}

passport.use(
  new GoogleStrategy(
    {
      clientID: keys.googleClientID,
      clientSecret: keys.googleClientSecret,
      callbackURL: "/auth/google/callback",
      proxy: true
    },
    async (accessToken, refreshToken, profile, done) => {
      const googleUser = await findGoogleUser(profile);
      console.log("GOOGLE USER");
      if (googleUser) {
        console.log("EXISTS");
        console.log(googleUser);
        done(null, googleUser);
      } else {
        console.log("does not exist");
        const newGoogleUser = await saveGoogleUser(profile);
        console.log(newGoogleUser);
        done(null, newGoogleUser);
      }
    }
  )
);
