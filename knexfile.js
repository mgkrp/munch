const keys = require("./config/keys");

const hostURL = "localhost";
const databaseName = "munch";

module.exports = {
  client: "pg",
  connection: {
    host: process.env.DATABASE_URL || hostURL,
    port: "5432",
    user: process.env.DB_USER || keys.databaseUser,
    password: process.env.DB_PASS || keys.databasePassword,
    database: databaseName
  },
  migrations: {
    directory: __dirname + "/db/migrations"
  },
  seeds: {
    directory: __dirname + "/db/seeds"
  }
};
