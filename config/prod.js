module.exports = {
  googleClientID: process.env.GOOGLE_CLIENT_ID,
  googleClientSecret: process.env.GOOGLE_CLIENT_SECRET,
  databaseUser: process.env.DB_USER,
  databasePassword: process.env.DB_PASS,
  cookieKey: process.env.COOKIE_KEY
}
