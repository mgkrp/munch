
exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('users').del()
    .then(function () {
      // Inserts seed entries
      return knex('users').insert([
        {g_id: 113123131, first_name: 'Livius', last_name: 'Amery', email: 'liviusamery@mailinator.com'},
        {g_id: 113123132, first_name: 'Travis', last_name: 'Waters', email: 'traviswaters@mailinator.com'},
        {g_id: 113123133, first_name: 'Blythe', last_name: 'Sierra', email: 'blythesierra@mailinator.com'}
      ]);
    });
};
